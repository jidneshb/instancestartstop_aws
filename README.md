# InstanceStartStop_AWS
# Stop Instance Python Script for AWS Lambda 
This script will help you to stop a running Instance from your account. 

The script is based on the Tag values to identify the Instance, which is to be deleted. 

Key  | Value
------------- | -------------
Type  | TestMachine

So you can add this Tag to mark a machine to shutdown automatically. 

# How to trigger a Lambda function 
To trigger your Lambda function, you need to set a **Cloudwatch Events**. 
Go to CloudWatch -> Events and Click on Rules.
 
In Step-1, you have to set up a schedule, to trigger the Lambda function.  

**E.g.** You have to schedule to trigger the Lambda function every 50th minute of the hours between 16-21 Hrs everyday.

```bash
50 16-21 * * ? *
```  
```bash
Tue, 10 Sep 2019 16:50:00 GMT
Tue, 10 Sep 2019 17:50:00 GMT
. . .
Wed, 11 Sep 2019 16:50:00 GMT
Wed, 11 Sep 2019 17:50:00 GMT
. . .
So above Schedule will trigger the Lambda on above GMT Time 
```
 - [ ] **The time in Scheduler is in GMT TZ**

### Steps Captures 
##### Step-1
![](Step-1CWEvents.PNG)

##### Step-2
![](Step-2CWEvents.PNG)

