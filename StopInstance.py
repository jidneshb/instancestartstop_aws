import boto3

client = boto3.client('ec2')

def lambda_handler(event, context):
    
    response = client.describe_instances()
    id1 = []

    for k in response['Reservations']:
        for lst in k.get('Instances')[0].get('Tags'):
            if lst['Value'] == 'TestMachine' and  k.get('Instances')[0].get('State').get('Name') == 'running':
               # print lst['Value'],"<===>", k.get('Instances')[0].get('InstanceId')
                id1.append(k.get('Instances')[0].get('InstanceId'))
                #print id1
                rsp = client.stop_instances(InstanceIds=[id1[0]])
    
    return("Completed")

